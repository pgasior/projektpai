<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AddpostController
 *
 * @author Piotrek
 */
class PostController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function add($category_name) {
        Authorization::checkAuthentication();
        if (CategoryModel::checkIfCategoryExistsByName($category_name)) {
            $this->View->render('post/add', array(
                'category_name' => $category_name
            ));
        } else {
            $this->View->render('post/adderror');
        }
    }
    
    public function add_post_action() {
        Authorization::checkAuthentication();
        $category_name = filter_input(INPUT_POST, 'category_name', FILTER_SANITIZE_STRING); 
        if (CategoryModel::checkIfCategoryExistsByName($category_name)) {
            $category_id = CategoryModel::getCategoryIdByName($category_name);
            $post_id = PostModel::addNewPost($category_id);
            Redirect::to('post/show/'.$post_id);
        } else {
            $this->View->render('post/adderror');
        }
    }

    public function show($post_id) {
        $post = PostModel::getPostById($post_id);
        if ($post) {
            $this->View->render('post/show', array(
                'post' => $post,
                'comments' => CommentModel::getAllComments($post_id)
                    )
            );
        } else {
            $this->View->render('post/error');
        }
    }
    
    public function add_comment_main($post_id) {
        Authorization::checkAuthentication();
        if (PostModel::checkIfPostExists($post_id)) {
            $this->View->render('comment/add', array(
                'post_id' => $post_id
            ));
        } else {
            $this->View->render('comment/adderror');
        }
    }

    public function add_comment_main_action($post_id) {
        
        Authorization::checkAuthentication();

        if (PostModel::checkIfPostExists($post_id)) {
                    
            CommentModel::addCommentToPost($post_id);
            Redirect::to('post/show/'.$post_id);
        } else {
            $this->View->render('comment/adderror');
        }
    }
    


    //put your code here
}
