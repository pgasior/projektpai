<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndexController
 *
 * @author Piotrek
 */
class IndexController extends Controller{
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $posts = PostModel::getLatestPostsByPopulatiry();
        $this->View->render('index/index', array(
            'posts' => $posts
        ));
    }
    
    public function test_mail() {
        $this->send_email("rayman112@gmail.com", "Tytul", "Tresc maila", "pgasior94@gmail.com", "pgasior94@gmail.com");
        echo "wyslalem";
        die();
    }
    
    public function test_mail_class() {
    	$mail = new PHPMailer;
    	$mail->setFrom('pgasior94@gmail.com', 'First Last');
    	//Set an alternative reply-to address
    	
    	//Set who the message is to be sent to
    	$mail->addAddress('rayman112@gmail.com', 'John Doe');
    	//Set the subject line
    	$mail->Subject = 'PHPMailer mail() test';
    	//Read an HTML message body from an external file, convert referenced images to embedded,
    	//convert HTML into a basic plain-text alternative body
    	$mail->msgHTML("Test");
    	//Replace the plain text body with one created manually
    	$mail->AltBody = 'This is a plain-text message body';
    	//Attach an image file
    	
    	//send the message, check for errors
    	if (!$mail->send()) {
    		echo "Mailer Error: " . $mail->ErrorInfo;
    	} else {
    		echo "Message sent!";
    	}
    	die();
    }
    
    private function mail_escape_header($subject) {
        $subject = preg_replace('/([^a-z ])/ie', 'sprintf("=%02x",ord(StripSlashes("\1")))', $subject);
        $subject = str_replace(' ', '_', $subject);
        return "=?utf-8?Q?$subject?=";
    }

    public function send_email($email, $title, $src, $reply_email, $from_user) {

        //$from_user = mail_escape_header($from_user);
        $from_user = filter_var($from_user, FILTER_SANITIZE_EMAIL);

        $headers = "From:$from_user <mail@adres.edu.pl>\r\nX-Mailer:mailer";
        $headers = $headers . "r\nReply-To:$reply_email\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Transfer-Encoding: 8bit\n";

        mail($email, $this->mail_escape_header($title), $src, $headers);
    }

}
