<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryController
 *
 * @author Piotrek
 */
class CategoryController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function categorylist() {
        $categories = CategoryModel::getAllCategoriesBySubscriptinsCount();
        $subscriptions = array();
        if (Session::userIsLoggedIn()) {
            $subscriptions = UserModel::getUserSubscriptionsByUsername(Session::get('username'));
        }
        $this->View->render('category/list', array(
            'categories' => $categories,
            'subscriptions' => $subscriptions
        ));
    }

    public function subscribe_ajax() {
        if (!Session::userIsLoggedIn()) {
            echo 0;
            return;
        }
        $category_id = filter_input(INPUT_POST, 'category_id',FILTER_SANITIZE_NUMBER_INT);
        if (CategoryModel::addSubscriptionForUser($category_id)) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function unsubscribe_ajax() {
        if (!Session::userIsLoggedIn()) {
            echo 0;
            return;
        }
        $category_id = filter_input(INPUT_POST, 'category_id',FILTER_SANITIZE_NUMBER_INT);
        if(CategoryModel::removeSubscriptionForUser($category_id)) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function show($name) {
        if(!CategoryModel::checkIfCategoryExistsByName($name)) {
            $this->View->render('category/not_found');
        } else {
            //var_dump(CategoryModel::getAllPostsInCategory($id));
            //echo CategoryModel::getCategoryIdByName($name);
            //die();
            $posts = CategoryModel::getAllPostsInCategory($name);
            $this->View->render('category/show',array(
                'posts' => $posts,
                'ID_categories' => CategoryModel::getCategoryIdByName($name),
                'category' => CategoryModel::getCategoryByName($name)
                )
            );
        }
    }
    
    
    
    

}
