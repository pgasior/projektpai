<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CommentController
 *
 * @author Piotrek
 */
class CommentController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function add_comment($comment_id,$post_id) {
        Authorization::checkAuthentication();
        if (PostModel::checkIfPostExists($post_id)) {
            $this->View->render('comment/add', array(
                'comment_id' => $comment_id,
                'post_id' => $post_id
            ));
        } else {
            $this->View->render('comment/adderror');
        }
    }

    public function add_comment_action($comment_id,$post_id) {

        Authorization::checkAuthentication();

        if (CommentModel::checkIfCommentExists($comment_id) && PostModel::checkIfPostExists($post_id)) {

            CommentModel::addCommentToComment($comment_id,$post_id);
            Redirect::to('post/show/' . $post_id);
        } else {
            $this->View->render('comment/adderror');
        }
    }

    //put your code here
}
