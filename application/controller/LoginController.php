<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LoginController extends Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        if(LoginModel::isUserLoggedIn()) {
            Redirect::home();
        } else {
            $this->View->render('login/index');
        }
    }

    public function login() {
        if(LoginModel::isUserLoggedIn()) {
            Redirect::home();
        }
        $username = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
        $login_sucessful = LoginModel::login($username, $password);
        if ($login_sucessful) {
            Redirect::home();
        } else {
            Redirect::to('login/index');
        }
    }

    public function logout() {
        LoginModel::logout();
        
        Redirect::home();
        exit();
    }

}
