<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RegisterController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        //Authorization::checkAuthentication();
        if (!LoginModel::isUserLoggedIn()) {
            $this->View->render('register/index');
        }
        else {
            Redirect::home();
        }
    }
    

    public function register_action() {
        $registration_succesful = RegisterModel::registerNewUser();
        if ($registration_succesful) {
            Message::addPositive("Zarejestrowano");
            Redirect::home();
        } else {
            $_SESSION['tmp_username'] = $_POST['username'];
            Redirect::to('register/index');
            //$this->register_action();
        }
    }
    
    public function check_login_ajax() {
    	$login = filter_input(INPUT_POST,'login', FILTER_SANITIZE_STRING);
    	if(UserModel::doesUserAlreadyExist($login)) {
    		echo '0';
    	} else {
    		echo '1';
    	}
    }
    
    public function verify($user_id, $user_activation_verification_code)
    {
    	if (isset($user_id) && isset($user_activation_verification_code)) {
    		RegisterModel::verifyNewUser($user_id, $user_activation_verification_code);
    		
    	}
    	Redirect::home();
    }

}
