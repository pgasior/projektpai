<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProfileController
 *
 * @author Piotrek
 */
class ProfileController extends Controller {

    public function __construct() {
        parent::__construct();
        
    }

    public function index() {
        Authorization::checkAuthentication();
        Redirect::to('profile/show/'.Session::get('username'));
    }

    public function show($username) {
        $user_data = UserModel::getUserDataByUsername($username);
        if($user_data) {
            $user_id = $user_data->ID_users;
            $user_statistics = UserModel::getUserStatistics($user_id);
            $this->View->render('profile/show', array(
                'user' => $user_data,
                'statistics' => $user_statistics
            ));
        } else {
            $this->View->render('profile/not_found');
        }
        
    }

    //put your code here
}
