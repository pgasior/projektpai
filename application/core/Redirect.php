<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Redirect
 *
 * @author Piotrek
 */
class Redirect {

    public static function home() {
        header("location: " . Config::get('URL'));
    }

    public static function to($path) {
        header("location: " . Config::get('URL') . $path);
    }

}
