<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Authorization
 *
 * @author Piotrek
 */
class Authorization {
    public static function checkAuthentication() {
        Session::init();
        if(!Session::userIsLoggedIn()) {
            Session::destroy();
            Redirect::to('login');
            exit();
        }
    }
}
