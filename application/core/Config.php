<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Config {
    private static $config;
    
    public static function get($key) {
        if(!self::$config) {
            $config_file = '../application/config/config.php';
            self::$config = require $config_file;
        }
        
        return self::$config[$key];
    }
}

