<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of View
 *
 * @author Piotrek
 */
class View {

    public function render($filename, $data = null) {
        if ($data) {
            foreach ($data as $key => $value) {
                $this->{$key} = $value;
            }
        }
        
                header("Cache-Control: no-store, no-cache, must-revalidate");  
header("Cache-Control: post-check=0, pre-check=0, max-age=0", false);
header("Pragma: no-cache");

        require Config::get('PATH_VIEW') . '_templates/header.php';
        require Config::get('PATH_VIEW') . $filename . '.php';
        require Config::get('PATH_VIEW') . '_templates/footer.php';
    }

    public function renderMulti($filenames, $data = null) {
        if (!is_array($filenames)) {
            $this->render($filenames, $data);
            return flase;
        }
        if ($data) {
            foreach ($data as $key => $value) {
                $this->{$key} = $value;
            }
        }

        require Config::get('PATH_VIEW') . '_templates/header.php';
        foreach ($filenames as $filename) {
            require Config::get('PATH_VIEW') . $filename . '.php';
        }
        require Config::get('PATH_VIEW') . '_templates/footer.php';
    }

    public function renderWithoutHeaderAndFooter($filename, $data = null) {
        if ($data) {
            foreach ($data as $key => $value) {
                $this->{$key} = $value;
            }
        }
        require Config::get('PATH_VIEW') . $filename . '.php';
    }
    
    public function renderMessages() {
        require Config::get('PATH_VIEW') . '_templates/feedback.php';
        Message::clear();
    }
    
    public function renderNavBar() {
        require Config::get('PATH_VIEW') . '_templates/navbar.php';
    }

}
