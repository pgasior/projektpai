<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SessionHandler
 *
 * @author Piotrek
 */
class MySessionHandler implements SessionHandlerInterface {

    /** @var $db mysqli */
    private $db;
    public $accessTime;

    public function __construct() {
        $this->db = DatabaseFactory::getFactory()->getConnection();
        $this->accessTime = time();
    }

    public function close() {
        return true;
    }

    public function destroy($session_id) {
        $stmt = $this->db->prepare("DELETE FROM session WHERE ID_session = ?");
        $stmt->bind_param('s',$session_id);
        $stmt->execute();
        return true;
    }

    public function gc($maxlifetime) {
        $old = time()-$maxlifetime;
        $stmt = $this->db->prepare("DELETE FROM session WHERE session_last_access < ?");
        $stmt->bind_param('s',$old);
        $stmt->execute();
        return true;
    }

    public function open($save_path, $name) {
        $save_path = '';
        $name = '';
        return true;
    }

    public function read($session_id) {
        $data = $this->readSessionData($session_id);
        if($data!=false) {
            return $data;
        }
        return '';
        
    }

    public function write($session_id, $session_data) {
        $this->writeSessionData($session_id,$session_data);
    }

    private function readSessionData($session_id) {
        $stmt = $this->db->prepare("SELECT session_data from session where ID_session = ?");
        $stmt->bind_param('s',$session_id);
        if($stmt->execute()) {
            $stmt->store_result();
            $data = '';
            $stmt->bind_result($data);
            $stmt->fetch();
            return $data;
        }
        return false;
    }

    private function writeSessionData($session_id, $session_data) {
        $stmt = $this->db->prepare("REPLACE INTO session "
                . "(ID_session,session_data, session_last_access) "
                . "values (?, ?, ?)");
        $stmt->bind_param('sss',$session_id,$session_data, $this->accessTime);
        $stmt->execute();
    }

//put your code here
}
