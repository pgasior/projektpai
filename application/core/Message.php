<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Message
 *
 * @author Piotrek
 */
class Message {
    static function addPositive($message) {
        Session::add('feedback_positive', $message);
    }
    static function addNegative($message) {
        Session::add('feedback_negative', $message);
    }
    static function clear() {
        Session::unset_key('feedback_positive');
        Session::unset_key('feedback_negative');
    }
    static function getPositive() {
        return Session::get('feedback_positive');
    }
    static function getNegative() {
        return Session::get('feedback_negative');
    }
}
