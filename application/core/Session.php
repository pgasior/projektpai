<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Session
 *
 * @author Piotrek
 */
class Session {
    
    private static $session_created = false;
    private static $handler;

    public static function init() {
        //if (session_id() == '') {
        if(!self::$session_created) {
            self::$handler = new MySessionHandler();
            session_set_save_handler(self::$handler, true);
            session_start();
            self::$session_created = true;       
        }
    }

    public static function XSSFilter(&$value) {
        if (is_string($value)) {
            $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
        }
        return $value;
    }

    public static function set($key, $value) {
        //debug_print_backtrace();
        $_SESSION[$key] = $value;
    }

    public static function get($key) {
        if (isset($_SESSION[$key])) {
            self::XSSFilter($_SESSION[$key]);
            return $_SESSION[$key];
        }
    }

    public static function destroy() {
        session_destroy();
    }

    public static function userIsLoggedIn() {
        return (self::get('user_logged_in') ? true : false);
    }
    
    public static function add($key,$value) {
        $_SESSION[$key][] = $value;
    }
    
    public static function unset_key($key){
        unset($_SESSION[$key]);
    }

}
