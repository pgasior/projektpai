<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DatabaseFactory
 *
 * @author Piotrek
 */
class DatabaseFactory {
    private static $factory;
    private $database;
    
    /*
     * @return DatabaseFactory
     */
    public static function getFactory() {
        if(!self::$factory) {
            self::$factory = new DatabaseFactory();
        }
        return self::$factory;
    }

    public function getConnection() {
        if(!$this->database) {
            $this->database = new mysqli(Config::get('DB_HOST'),  Config::get('DB_USER'),  Config::get('DB_PASS'),  Config::get('DB_NAME'));
        }
        return $this->database;
    }
}
