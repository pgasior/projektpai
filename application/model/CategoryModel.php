<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryModel
 *
 * @author Piotrek
 */
class CategoryModel {
    public static function getAllCategoriesBySubscriptinsCount() {
        $db = DatabaseFactory::getFactory()->getConnection();
        $result = $db->query("select * from categories order by category_subscription_count DESC");
        $categories = $result->fetch_all(MYSQLI_ASSOC);
        return $categories;
    }
    public static function addSubscriptionForUser($category_id) {
        $user_id= Session::get('user_id');
        $db = DatabaseFactory::getFactory()->getConnection();
        if(!self::checkIfCategoryExistsById($category_id)) {
            Message::addNegative('Nie ma takiej kategorii '.$category_id);
            return false;
        }
        if(self::checkIfAlreadySubscribed($category_id)) {
            Message::addNegative('Już subskrybujesz tą kategorie');
            return false;
        }
        $stmt = $db->prepare("INSERT INTO categories_subscriptions (ID_users, ID_categories) VALUES (?,?)");
        $stmt->bind_param("ii", $user_id, $category_id);
        return $stmt->execute();
    }
    
    public static function removeSubscriptionForUser($category_id) {
        $user_id= Session::get('user_id');
        $db = DatabaseFactory::getFactory()->getConnection();
        if(!self::checkIfCategoryExistsById($category_id)) {
            Message::addNegative('Nie ma takiej kategorii');
            return false;
        }
        if(!self::checkIfAlreadySubscribed($category_id)) {
            Message::addNegative('Nie sybskrybujesz tej kategorii');
            return false;
        }
        $stmt = $db->prepare("delete from categories_subscriptions where ID_users = ? AND ID_categories = ?");
        $stmt->bind_param("ii", $user_id, $category_id);
        return $stmt->execute();
    }
    
    public static function checkIfCategoryExistsByName($category_name) {
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT ID_categories FROM categories WHERE category_name = ?");
        $stmt->bind_param('i',$category_name);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows==0) {
            return false;
        }
        return true;
    }
    
    public static function checkIfCategoryExistsById($category_id) {
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT ID_categories FROM categories WHERE ID_categories = ?");
        $stmt->bind_param('i',$category_id);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows==0) {
            return false;
        }
        return true;
    }
    
    public static function checkIfAlreadySubscribed($category_id) {
        $db = DatabaseFactory::getFactory()->getConnection();
        $user_id = Session::get('user_id');
        $stmt = $db->prepare("SELECT ID_categories "
                . "FROM categories_subscriptions "
                . "WHERE ID_categories = ? AND ID_users = ?");
        $stmt->bind_param('ii',$category_id,$user_id);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows==0) {
            return false;
        }
        return true;
    }
    
    public static function getAllPostsInCategory($category_name) {
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("select posts.*, post_statistics.* "
                . "from posts "
                . "natural join categories "
                . "natural join post_statistics "
                . "natural join users "
                . "where category_name=?;");
        $stmt->bind_param('s',$category_name);
        $stmt->execute();
        $result = $stmt->get_result();
        $posts = $result->fetch_all(MYSQLI_ASSOC);
        return $posts;
    }
    
    public static function getCategoryIdByName($name) {
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT ID_categories FROM categories WHERE category_name = ?");
        $stmt->bind_param('s',$name);
        $stmt->execute();
        $result = $stmt->get_result();
        $category = $result->fetch_assoc();
        return $category['ID_categories'];
    }
    
    public static function getCategoryByName($name) {
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT * FROM categories WHERE category_name = ?");
        $stmt->bind_param('s',$name);
        $stmt->execute();
        $result = $stmt->get_result();
        $category = $result->fetch_assoc();
        return $category;
    }
    
}
