<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PostModel
 *
 * @author Piotrek
 */
class PostModel {
    public static function addNewPost($category_id) {
        $post_title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
        $post_link = filter_input(INPUT_POST, 'post_link', FILTER_SANITIZE_STRING);
        $post_contents = filter_input(INPUT_POST, 'post_contents', FILTER_SANITIZE_STRING);
        $validation = self::add_post_validation($post_title,$post_link,$post_contents);
        if(!$validation) {
            return false;
        }
        $user_id=Session::get('user_id');
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("INSERT INTO posts (ID_users, ID_categories, post_contents, post_url, post_title) VALUES (?,?,?,?,?)");
        $stmt->bind_param("iisss", $user_id, $category_id,$post_contents,$post_link,$post_title);
        $stmt->execute();
        //echo $db->error;
        //die();
        return $db->insert_id;
    }
    
    public static function getLatestPostsByPopulatiry() {
        /* @var $db mysqli */
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("select ID_posts, post_url, post_title, username, category_name, post_score, post_date, post_comments,
post_last_update_time
from posts natural join post_statistics natural join categories natural join users order by post_score desc;");
        $stmt->execute();
        $result = $stmt->get_result();
        
        $posts = array();
        while($row = $result->fetch_assoc()) {
            $posts[] = $row;
        }
        return $posts;
    }

    public static function add_post_validation($post_title, $post_link, $post_contents) {
        if(empty($post_title)) {
            Message::addNegative("Nie podano tytułu");
            return false;
        }
        if(empty($post_link)) {
            Message::addNegative("Nie podano linka");
            return false;
        }
        return true;
    }
    
    public static function getPostById($post_id) {
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT * FROM posts WHERE ID_posts = ?");
        $stmt->bind_param('i',$post_id);
        $stmt->execute();
        $result = $stmt->get_result();
        //var_dump($result->fetch_object());
        //die();
        if($result->num_rows==0) {
            return false;
        }
        return $result->fetch_object();
    }

    public static function checkIfPostExists($post_id) {
        /* @var $db mysqli */
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT ID_posts FROM posts WHERE ID_posts = ?");
        $stmt->bind_param('i', $post_id);
        $stmt->execute();
        $result = $stmt->get_result();
       if($result->num_rows==0) {
            return false;
        }
        return true;
    }

}
