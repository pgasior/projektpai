<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RegisterModel {

    public static function registerNewUser() {
        $user_name = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $user_email = filter_input(INPUT_POST, 'user_email',FILTER_SANITIZE_EMAIL);
        $user_password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
        $password_confirmation = filter_input(INPUT_POST, 'confirmpwd', FILTER_SANITIZE_STRING);
        

        $validation = self::registrationValidation($user_name, $user_email, $user_password, $password_confirmation);
        if(!$validation){
            return false;
        }
        if(UserModel::doesUserAlreadyExist($user_name)) {
            Message::addNegative("Nazwa użytkownika zajęta");
            return false;
        }
        $password_hashed = password_hash($user_password, PASSWORD_DEFAULT);
        
        $activation_hash = sha1(uniqid(mt_rand(), true));
        $state = self::writeNewUserToDatabase($user_name, $user_email, $password_hashed, $activation_hash);
        
        $user_id = UserModel::getUserIdByUsername($user_name);
        self::sendVerificationEmail($user_id, $user_email, $activation_hash);

        return $state;
    }

    public static function writeNewUserToDatabase($user_name,$email, $password_hashed, $activation_hash) {
        /* @var $db mysqli */
        $db = DatabaseFactory::getFactory()->getConnection();
        //$stmt = $db->prepare("INSERT INTO users (username, password)VALUES (?,?)");
        $stmt = $db->prepare("CALL add_user(?,?,?, ?)");
        $stmt->bind_param("ssss", $user_name, $email, $password_hashed, $activation_hash);
        return $stmt->execute();
    }

    public static function registrationValidation($user_name, $email, $user_password, $password_confirmation) {
        if (self::validateUserName($user_name) 
        		AND self::validateUserPassword($user_password, $password_confirmation)
        		AND self::validateEmail($email)) {
            return true;
        }
        return false;
    }
    
    public static function validateEmail($email) {
    	if(empty($email)) {
    		Message::addNegative("Nie podano adresu email");
    		return false;
    	}
    	if(!filter_var($email,FILTER_VALIDATE_EMAIL)) {
    		Message::addNegative("Email niepoprawny");
    		return false;
    	} 
    	if(UserModel::doesEmailAlreadyExist($email)) {
    		Message::addNegative("Email znajduje sie juz w bazie");
    		return false;
    	}
    	return true;
    }

    public static function validateUserPassword($user_password, $password_confirmation) {
        if (empty($user_password)) {
            Message::addNegative("Nie podano hasła");
            return false;
        }
        if (strlen($user_password) < 6) {
            Message::addNegative("Hasło musi mieć 6 lub więcej znaków");
            return false;
        }
        if ($user_password !== $password_confirmation) {
            Message::addNegative("Hasła nie pasują do siebie");
            return false;
        }
        
        return true;
    }

    public static function validateUserName($user_name) {
        if (empty($user_name)) {
            Message::addNegative("Nie podano nazwy uzytkownika");
            return false;
        }
        if (!preg_match('/^[a-zA-Z0-9]{2,30}$/', $user_name)) {
            Message::addNegative("Nazwa użytkownika nie pasuje do wzorca");
            return false;
        }
        return true;
    }
	private static function sendVerificationEmail($user_id, $user_email, $user_activation_hash) {
		$body = "Kliknij w link aby aktywować konto: " . Config::get ( 'URL' ) . 'register/verify/' . urlencode ( $user_id ) . '/' . urlencode ( $user_activation_hash );
		$mail = new PHPMailer ();
		$mail->setFrom ( 'pgasior94@gmail.com', 'First Last' );

		$mail->addAddress ( $user_email);
		// Set the subject line
		$mail->Subject = 'Aktywacja konta';
		// Read an HTML message body from an external file, convert referenced images to embedded,
		// convert HTML into a basic plain-text alternative body
		$mail->msgHTML ( $body );
		// Replace the plain text body with one created manually
		$mail->AltBody = $body;
		// Attach an image file
		$mail->send ();
		

    }
    
    public static function verifyNewUser($user_id, $user_activation_verification_code) {
    	$db = DatabaseFactory::getFactory()->getConnection();
    	$stmt = $db->prepare("UPDATE users SET activated = 1, activation_hash= NULL 
    			where ID_users = ? AND activation_hash=?");
    	$stmt->bind_param('ss',$user_id, $user_activation_verification_code);
    	
    	$stmt->execute();
    	if($stmt->affected_rows == 0) {
    		Message::addNegative("Aktywacja nieudana");
    		return false;
    	}
    	Message::addPositive("Aktywowano");
    	return true;
    }

}
