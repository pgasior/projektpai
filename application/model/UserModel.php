<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserModel
 *
 * @author Piotrek
 */
class UserModel {
    public static function doesUserAlreadyExist($username) {
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT ID_users FROM users WHERE username = ?");
        $stmt->bind_param('s',$username);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows==0) {
            return false;
        }
        return true;
    }
    
    public static function getUserStatistics($user_id) {
        /* @var $db mysqli */
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT * from users_statistics WHERE ID_users = ?");
        $stmt->bind_param('i',$user_id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_object();
    }

  
    public static function getUserDataByUsername($username) {
        /* @var $db mysqli  */
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT * "
                . "FROM users WHERE username = ?");
        $stmt->bind_param("s",$username);
        $stmt->execute();
        $result = $stmt->get_result();
        //echo $db->error.'<br/>';
        return $result->fetch_object();
    }
    
    public static function getUserSubscriptionsByUsername($username) {
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("select category_name from subscriptions_username where username = ?");
        $stmt->bind_param("s",$username);
        $stmt->execute();
        $result = $stmt->get_result();
        
        $subscriptions = array();
        while($row = $result->fetch_assoc()) {
            $subscriptions[] = $row['category_name'];
        }
        return $subscriptions;
    }
    
    public static function doesEmailAlreadyExist($user_email)
    {
    	echo 'Email: '.$user_email.'</br>';
    	$database = DatabaseFactory::getFactory()->getConnection();
    	$query = $database->prepare("SELECT ID_users FROM users WHERE email = ? LIMIT 1");
    	$query->bind_param('s', $user_email);
    	$query->execute();
    	$query->store_result();
    	if ($query->num_rows == 0) {
    		return false;
    	}
    	echo $database->error.'</br>';
    	return true;
    }
    
    public static function getUserIdByUsername($user_name)
    {
    	$database = DatabaseFactory::getFactory()->getConnection();
    	$sql = "SELECT ID_users FROM users WHERE username = ? LIMIT 1";
    	$query = $database->prepare($sql);
    	$query->bind_param('s', $user_name);
    	$query->execute();
    	$result = $query->get_result();
    	$user = $result->fetch_assoc();
    	return $user['ID_users'];
    	
    }

}
