<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CommentModel {
    public static function addCommentToPost($post_id) {
        $comment_content = filter_input(INPUT_POST, 'comment_content', FILTER_SANITIZE_STRING);
        $user_id = Session::get('user_id');
        $validation = self::validateComment($comment_content);
        if(!$validation){
            return false;
        }
        /* @var $db mysqli */
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("INSERT INTO comments (ID_users,ID_posts,comment_content) values (?,?,?);");
        $stmt->bind_param('iis', $user_id,$post_id,$comment_content);
        $stmt->execute();
        return true;
        
    }

    private static function validateComment($comment_content) {
        if(!isset($comment_content) || strlen($comment_content)==0) {
            Message::addNegative('Nie podano tresci komentarza');
            return false;
        }
        
        return true;
    }
    
    public static function getAllComments($post_id) {
        /* @var $db mysqli */
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT * from comments where ID_posts = ? AND ID_parent IS NULL;");
        $stmt->bind_param('i',$post_id);
        $stmt->execute();
        $result = $stmt->get_result();
        
        $comments = array();
        $i=0;
        while($row = $result->fetch_assoc()) {
            $comments[$i] = $row;
            $i++;
            
        }
        foreach($comments as &$comment) {
            $comment['children'] = self::getAllSubcomments($comment['ID_comments']);
        }
        return $comments;
    }
    
    private static function getAllSubcomments($parent_id) {
        /* @var $db mysqli */
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT * from comments where ID_parent = ?;");
        $stmt->bind_param('i',$parent_id);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows == 0) {
            return null;
        }
        $children = array();
        $i=0;
        while($row = $result->fetch_assoc()) {
            $children[$i] = $row;
            $i++;
            
        }
        foreach($children as &$child) {
            $child['children'] = self::getAllSubcomments($child['ID_comments']);
        }
        return $children;
    }

    public static function checkIfCommentExists($comment_id) {
        /* @var $db mysqli */
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("SELECT ID_comments FROM comments WHERE ID_comments = ?");
        $stmt->bind_param('i', $comment_id);
        $stmt->execute();
        $result = $stmt->get_result();
       if($result->num_rows==0) {
            return false;
        }
        return true;
    }

    public static function addCommentToComment($comment_id,$post_id) {
        $comment_content = filter_input(INPUT_POST, 'comment_content', FILTER_SANITIZE_STRING);
        $user_id = Session::get('user_id');
        $validation = self::validateComment($comment_content);
        if(!$validation){
            return false;
        }
        /* @var $db mysqli */
        $db = DatabaseFactory::getFactory()->getConnection();
        $stmt = $db->prepare("INSERT INTO comments (ID_users,ID_parent, ID_posts,comment_content) values (?,?,?,?);");
        $stmt->bind_param('iiis', $user_id,$comment_id,$post_id,$comment_content);
        $stmt->execute();
        return true;
    }

}