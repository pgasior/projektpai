<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginModel
 *
 * @author Piotrek
 */
class LoginModel {

    public static function isUserLoggedIn() {
        return Session::userIsLoggedIn();
    }
//TODO: auto logowanie na cookie
    public static function login($username, $password) {
        if (empty($username) OR empty($password)) {
            Message::addNegative('Nie podano nazwy użytkownika lub hasła');
            return false;
        }

        $result = self::validateAndGetUser($username, $password);

        if (!$result) {
            return false;
        }
        self::setSuccessfulLoginIntoSession(
                $result->ID_users, $result->username
        );
        return true;
    }
//TODO: zabezpieczenia przed brute force
    public static function validateAndGetUser($username, $password) {
        $result = UserModel::getUserDataByUsername($username);
        if(!$result) {
            Message::addNegative("Zła nazwa użytkownika lub hasło");
            return false;
        }
        if(!password_verify($password, $result->password)) {
            Message::addNegative("Zła nazwa użytkownika lub hasło");
            return false;
        }
        if($result->activated == 0) {
        	//var_dump($result);
        	//die();
        	Message::addNegative("Konto nie zostało aktywowane");
        	return false;
        }
        
        return $result;
    }

    public static function setSuccessfulLoginIntoSession($id, $username) {
        Session::init();
        session_regenerate_id(true);
        $_SESSION = array();
        Session::set('user_id', $id);
        Session::set('username', $username);
        Session::set('user_logged_in',true);
    }

    public static function logout() {
        Session::destroy();
        
    }

}
