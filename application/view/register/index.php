<ul>
    <li>Nazwa uzytkownika moze zawierać tylko litery, cyfry i znaki podkreślenia</li>
    <li>Hasło musi mieć co najmniej 6 znaków</li>
</ul>
<div id="password_info"></div>
<div id="login_info"></div>
<form class="form-horizontal" role="form" 
      action="<?php echo Config::get('URL'); ?>register/register_action" 
      method="post" 
      name="registration_form"
      onsubmit="return form_submit(this);">
    <div class="form-group">
        <label class="control-label col-sm-2" for="username">Login:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="username" name="username" placeholder="Wprowadź login"
                   value="<?php if(isset($_SESSION['tmp_username'])){echo $_SESSION['tmp_username']; unset($_SESSION['tmp_username']);} ?>"
                   required onblur="check_login();">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Email:</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="user_email" placeholder="Wprowadź email"
                   value=""
                   required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="password">Hasło:</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="passwordd" name="password" onblur="check_password();" placeholder="Wprowadź hasło" required pattern=".{6,}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="confirmpwd">Powtórz haslo:</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="confirmpwd" name="confirmpwd" onblur="check_password();" placeholder="Powtórz haslo" required pattern=".{6,}">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" id="submit_button" disabled class="btn btn-default">Zarejestruj</button>
        </div>
    </div>
</form>

<script src="<?php echo Config::get('URL'); ?>js/my_ajax.js"></script>