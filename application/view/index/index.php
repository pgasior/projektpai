<?php use Carbon\Carbon; 
Carbon::setLocale('pl')
?>
<?php foreach ($this->posts as $post) { ?>
    <div class="panel panel-default subscribtion-panel">
        <div class="panel-heading">
            <a href="<?php echo Config::get('URL') . 'post/show/' . $post['ID_posts']; ?>"><b><?php echo $post['post_title']; ?></b></a> 
            <div style="float:right;"> <a href="<?php echo Config::get('URL').'profile/show/'.$post['username']; ?>"><?php echo $post['username']; ?></a> </div>
        </div>
        <div class="panel-body">
            <div class="text-muted">Dodano: <?php echo Carbon::parse($post['post_date'])->diffForHumans();?></div>
            <div class="text-muted">Kategoria: <a href="<?php echo Config::get('URL').'/category/show/'.$post['category_name']; ?>"><?php echo $post['category_name'] ?></a></div>
            <div class="text-muted">Wynik: <?php echo $post['post_score'] ?></div>
            <div class="text-muted">Liczba komentarzy: <?php echo $post['post_comments'] ?></div>
        </div>
    </div> 
    <?php
}
?>

<?php var_dump($this->posts); ?>