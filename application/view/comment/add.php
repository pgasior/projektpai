Dodaj nowy Komentarz
<?php
$link = "";
if(isset($this->comment_id)) {    
    $link = "comment/add_comment_action/".$this->comment_id.'/'.$this->post_id;
} else {
    $link = "post/add_comment_main_action/".$this->post_id;
}
?>
<form class="form-horizontal" role="form" 
      action="<?php echo Config::get('URL').$link; ?>"
      method="post" 
      name="new_comment_form">
    <div class="form-group">
        <label class="control-label col-sm-2" for="comment_content">Treść:</label>
        <div class="col-sm-10">
            <textarea  class="form-control" id="comment_content" name="comment_content" placeholder="Treść komentarza"></textarea>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Dodaj komentarz</button>
        </div>
    </div>

</form>