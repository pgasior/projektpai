
<style>
    .panel {
        margin-bottom: 5px;
    }
</style>

<div class="panel panel-default">
    <div class="panel-heading"><a href='<?php echo $this->post->post_url; ?>' ><?php echo $this->post->post_title; ?></a></div>
    <div class="panel-body"><div><?php echo $this->post->post_contents; ?> </div>
        <?php
        if (Session::userIsLoggedIn()) {
            echo "<div><a href ='" . Config::get('URL') . "post/add_comment_main/" . $this->post->ID_posts . "'>Dodaj komentarz</a></div>";
        }
        ?></div> 

</div>
<div>
    Komentarze
    <?php var_dump($this->comments); ?>
    <?php print_comments($this->comments, 0); ?>
</div>

<?php

function print_comments($comments, $indent_level) {
    for ($i = 0; $i < $indent_level; $i++) {
        echo '&nbsp;';
    }
    foreach ($comments as &$comment) {
        ?>
        <div class="panel panel-default">
            <div class="panel-body">
                <div>
                    <?php echo $comment['comment_content']; ?>
                </div>
                <div><a href="<?php echo Config::get('URL').'comment/add_comment/'.$comment['ID_comments'].'/'.$comment['ID_posts'];?>">Dodaj komantarz</a></div>
                <?php
                if (!is_null($comment['children'])) {
                    print_comments($comment['children'], $indent_level + 2);
                }
                ?>
            </div>

        </div>
    <?php
    }
}
?>
