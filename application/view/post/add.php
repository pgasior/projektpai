Dodaj nowy post do kategorii <?php echo $this->category_name."<br/>" ?>

<form class="form-horizontal" role="form" 
      action="<?php echo Config::get('URL'); ?>post/add_post_action" 
      method="post" 
      name="new_post_form">
    <div class="form-group">
        <label class="control-label col-sm-2" for="title">Tytuł:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="post-title" name="title" placeholder="Tytuł">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="post-link">Link:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="post-link" name="post_link" placeholder="Wprowadź hasło">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="post-contents">Zawartosć:</label>
        <div class="col-sm-10">
            <textarea  class="form-control" id="post-contents" name="post_contents" placeholder="Treść posta"></textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Dodaj post</button>
        </div>
    </div>
    
    <input type="hidden" name="category_name" value="<?php echo $this->category_name ?>">
</form>