<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo Config::get('URL'); ?>">Projekt</a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li ><a href="#">Popularne</a></li>
                <li ><a href="#">Nowe</a></li>
                <li ><a href="<?php echo Config::get('URL'); ?>category/categorylist">Kategorie</a></li>
                <?php if (Session::userIsLoggedIn()) : ?>
                    <li ><a href="#">Dodaj kategorie</a></li>
                    <li ><a href="#">Link 2 dla zalogowanego</a></li>
                    <li><a href="<?php echo Config::get('URL'); ?>profile">Profil</a></li>
                <?php endif; ?>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (!Session::userIsLoggedIn()) : ?>
                    <li><a href="<?php echo Config::get('URL'); ?>register"><span class="glyphicon glyphicon-user"></span> Rejestracja</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"><span class="glyphicon glyphicon-log-in"></span> Zaloguj się </a>
                        <div class="dropdown-menu" style="padding: 15px; padding-bottom: 15px; min-width: 200px;">
                            <form role="form" action="<?php echo Config::get('URL'); ?>login/login" method="post" name="login_form"
                                  onsubmit="encrypt_login_form(this);">
                                <div class="form-group">
                                    <label for="login">Login:</label>
                                    <input type="text" class="form-control" id="login" name="login" required>
                                </div>
                                <div class="form-group">
                                    <label for="password">Hasło:</label>
                                    <input type="password" class="form-control" id="password" name="password" required>
                                </div>
                                <button type="submit" class="btn btn-default" >Zaloguj</button>
                            </form>
                        </div>
                    </li>
                <?php else : ?>
                    <li><a href="<?php echo Config::get('URL'); ?>login/logout"><span class="glyphicon glyphicon-user"></span> Wyloguj</a></li>
                    <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>

<script>
$(document).ready(function(){
    $('.dropdown').on('shown.bs.dropdown', function(){
        $('#login').focus().select();
    });
});
</script>