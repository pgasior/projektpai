<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <form role="form" class="form-horizontal" action="<?php echo Config::get('URL'); ?>login/login" method="post" name="login_form"
              onsubmit="encrypt_login_form(this);">
            <div class="form-group">
                <label for="login" class="control-label col-sm-2">Login:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="login" name="login" required>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="control-label col-sm-2">Hasło:</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="password" name="password" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default" >Zaloguj</button>
                </div>
            </div>
        </form>
    </div>
</div>