

<?php foreach ($this->categories as $category) { ?>
    <?php
    $result = in_array($category['category_name'], $this->subscriptions);
    $btn_text = ($result ? 'Usuń subskrypcje' : 'Subskrybuj');
    $btn_class = "subscription-btn btn btn-sm ";
    $btn_class .= ($result ? 'btn-success' : 'btn-danger');
    ?>
    <!--<div class="row">-->
        <!--<div>-->
        <?php if (Session::userIsLoggedIn()) : ?>
        <div class="subscribe-div">
            <button id="cat_<?php echo $category['ID_categories'];?>" type="button" class="<?php echo $btn_class; ?>"><?php echo $btn_text; ?></button>
        </div>
        <?php endif; ?>
        
            <div class="panel panel-default subscribtion-panel">
                <div class="panel-heading"><a href=<?php echo Config::get('URL').'category/show/'.$category['category_name']; ?>><b><?php echo $category['category_name']; ?></b></a> </div>
                <div class="panel-body">
                    <?php echo $category['category_description']; ?> <br/>
                    <div class="text-muted">Liczba subskrypcji: <?php echo $category['category_subscription_count'] ?></div>
                </div>
            </div>
        
        <!--</div>-->
    <!--</div>-->
<?php } ?>

    <script src="<?php echo Config::get('URL'); ?>js/my_ajax.js"></script>
