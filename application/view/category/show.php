

<div class="row">
    <div class="col-xs-9">
        <?php foreach ($this->posts as $post) { ?>
            <div class="panel panel-default subscribtion-panel">
                <div class="panel-heading"><a href="<?php echo Config::get('URL').'post/show/'.$post['ID_posts']; ?>"><b><?php echo $post['post_title']; ?></b></a> </div>
                <div class="panel-body">
                    <?php echo $post['post_contents']; ?> <br/>
                    <div class="text-muted">Liczba komentarzy: <?php echo $post['post_comments'] ?></div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="col-xs-3" style="padding-left:0px;">
        <div class="well well-sm">
            <?php if (Session::userIsLoggedIn()) : ?>
                <?php
                $subscribed = CategoryModel::checkIfAlreadySubscribed($this->ID_categories);
                $btn_text = ($subscribed ? 'Usuń subskrypcje' : 'Subskrybuj');
                $btn_class = "subscription-btn btn btn-sm ";
                $btn_class .= ($subscribed ? 'btn-success' : 'btn-danger');
                ?>
                <button id="cat_<?php echo $this->ID_categories; ?>" type="button" class="<?php echo $btn_class; ?>"><?php echo $btn_text; ?></button><br/>
                <a class="btn" href="<?php echo Config::get('URL').'post/add/'.$this->category['category_name']; ?>">Nowy post</a>
            <?php endif; ?>
                <h2><?php echo $this->category['category_name'];?></h2>
            <?php
            
            
            echo $this->category['category_description'];
            ?>
        </div>
    </div>
</div>

<script src="<?php echo Config::get('URL'); ?>js/my_ajax.js"></script>


