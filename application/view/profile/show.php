<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $this->user->username ?></h3>
    </div>
    <div class="panel-body">
        Komentarze: <?php echo $this->statistics->comment_count; ?> <br/>
        Posty: <?php echo $this->statistics->post_count; ?>
    </div>
</div>
