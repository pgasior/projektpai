//$(".subscription-btn.btn-danger").click(function () {
//    $url = window.location.href;
//    $url = $url.replace(/categorylist/, "subscribe_ajax");
//    $category_id = this.id;
//    $category_id = $category_id.replace("cat_", "");
//
//    $.ajax({
//        method: "POST",
//        url: $url,
//        data: {category_id: $category_id}
//    }).done(function (reply) {
//        if (reply === "1") {
//            alert($category_id);
//            $("#cat_"+$category_id).toggleClass('btn-danger').toggleClass('btn-success').text("Usuń subskrypcję");
//        }
//    });
//});

$(".subscription-btn.btn-danger").click(function () {
    subscribe(this);
});
$(".subscription-btn.btn-success").click(function () {
    unsubscribe(this);
});

function subscribe($button) {
    $url = window.location.href;
    //$url = $url.replace(/categorylist/, "subscribe_ajax");
    $url = $url.replace(/category\/.*/, "category/subscribe_ajax");
    $category_id = $button.id;
    $category_id = $category_id.replace("cat_", "");

    $.ajax({
        method: "POST",
        url: $url,
        data: {category_id: $category_id}
    }).done(function (reply) {

        if (reply === "1") {
            //alert($category_id);
            $("#cat_" + $category_id)
                    .toggleClass('btn-danger')
                    .toggleClass('btn-success')
                    .text("Usuń subskrypcję")
                    .unbind("click")
                    .click(function () {
                        unsubscribe($button);
                    });
        }
    });
}

function unsubscribe($button) {
    $url = window.location.href;
    //$url = $url.replace(/categorylist/, "unsubscribe_ajax");
    $url = $url.replace(/category\/.*/, "category/unsubscribe_ajax");
    $category_id = $button.id;
    $category_id = $category_id.replace("cat_", "");

    $.ajax({
        method: "POST",
        url: $url,
        data: {category_id: $category_id}
    }).done(function (reply) {
        if (reply === "1") {
            //alert($category_id);
            $("#cat_" + $category_id)
                    .toggleClass('btn-success')
                    .toggleClass('btn-danger')
                    .text("Subskrybuj")
                    .unbind("click")
                    .click(function () {
                        subscribe($button);
                    });
        }
    });
}

function check_login() {
	$login = document.getElementById('username');
	//alert(username.value);
	var d = document.getElementById('login_info');
	$url = window.location.href;
    //$url = $url.replace(/categorylist/, "unsubscribe_ajax");
    $url = $url.replace(/register\/.*/, "register/check_login_ajax");
	$.ajax({
		method: "POST",
		url: $url,
		data: {login: $login.value}
	}).done(function(reply) {
		//d.innerHTML = reply;
		$btn = document.getElementById('submit_button');
		if(reply==="0") {
			$btn.disabled= true;
			d.innerHTML = "Login zajety";
		} else {
			$btn.disabled= false;
		}
	})
}

